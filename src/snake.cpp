#include "snake.h"
#include <vector>
#include <iostream>
#include <random>

snake_node::snake_node(position pos)
    : pos(pos), next(nullptr)
{
}
snake_node::snake_node(position pos, snake_node* next)
    : pos(pos), next(next)
{
}
snake_node::~snake_node()
{
    delete next;
}
snake_node* snake_node::operator=(const snake_node& copy)
{
    pos = copy.pos;
    next = copy.next;
    return this;
}


snake::snake(u16 width, u16 height)
    : _width(width), _height(height), alive(true), _round(0)
{
    _apple = {(u16)(height/2), (u16)(width/3)};
    _snake_head = new snake_node({(u16)(height/2), (u16)(width/3*2)});
}
snake::~snake()
{
    delete _snake_head;
}
void snake::update(direction direction)
{
    //calculate new head position
    position new_pos;
    switch(direction)
    {
        case direction::up:
            new_pos = {(u16)(_snake_head->pos.x + 1), (u16)(_snake_head->pos.y)};
            break;
        case direction::left:
            new_pos = {(u16)(_snake_head->pos.x), (u16)(_snake_head->pos.y + 1)};
            break;
        case direction::down:
            new_pos = {(u16)(_snake_head->pos.x - 1), (u16)(_snake_head->pos.y)};
            break;
        case direction::right:
            new_pos = {(u16)(_snake_head->pos.x), (u16)(_snake_head->pos.y - 1)};
            break;
    }

    //in the first 4 rounds...
    if(_round < 4)
    {
        _round++;
        _snake_head = new snake_node(new_pos, _snake_head);
        return;
    }

    cell next_cell = at(new_pos.x, new_pos.y);
    //if you bump into something
    if(new_pos.x < 0 || new_pos.y < 0 || new_pos.x >= _height || new_pos.y >= _width || next_cell == cell::snake_body)
    {
        alive = false;
        return;
    }
    if(next_cell == cell::apple)
    {
        snake_node* new_head = new snake_node(new_pos, _snake_head);
        _snake_head = new_head;
        //create new apple
        std::random_device os_seed;
        std::mt19937 generator(os_seed());
        std::uniform_int_distribution<u16> height_distribution(0, _height - 1);
        std::uniform_int_distribution<u16> width_distribution(0, _width - 1);
        position new_apple;
        do
        {
            new_apple = {height_distribution(generator), width_distribution(generator)};
        } while (at(new_apple.x, new_apple.y) != cell::nothing);
        _apple = new_apple;
        return;
    }

    //move forward
    if(_snake_head->next != nullptr)
    {
        snake_node* current = _snake_head;
        while(current->next->next != nullptr)
        {
            current = current->next;
        }
        delete current->next;
        current->next = nullptr;
    }
    else
    {
        delete _snake_head;
        _snake_head = nullptr;
    }
    snake_node* new_head = new snake_node(new_pos, _snake_head);
    _snake_head = new_head;
}
void snake::print()
{
    //set screen
    std::vector<std::vector<char>> screen;
    for(int x = 0; x < _height; x++)
    {
        screen.push_back(std::vector<char>());
        for(int y = 0; y < _width; y++)
            screen[x].push_back(' ');
    }
    
    screen[_apple.x][_apple.y] = 'A';
    snake_node* current = _snake_head;
    while(current != nullptr)
    {
        screen[current->pos.x][current->pos.y] = 'S';
        current = current->next;
    }
    //print
    for(int y = 0; y < _width + 2; y++)
        std::cout << '-';
    std::cout << "\n";
    for(int x = _height - 1; x >= 0; x--)
    {
        std::cout << "|";
        for(int y = _width - 1; y >= 0; y--)
            std::cout << screen.at(x).at(y);
        std::cout << "|\n";
    }
    for(int y = 0; y < _width + 2; y++)
        std::cout << '-';
    std::cout << "\n";
}
cell snake::at(u16 x, u16 y)
{
    if(_apple.x == x && _apple.y == y) return cell::apple;
    snake_node* current = _snake_head;
    while(current != nullptr)
    {
        if(current->pos.x == x && current->pos.y == y) return cell::snake_body;
        current = current->next;
    }
    return cell::nothing;
}