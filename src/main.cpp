#include "snake.h"

#include <iostream>
#include <thread>

int main()
{
    snake snake(40, 20);
    while(snake.alive)
    {
        snake.print();
        char key;
        std::cin >> key;
        direction direction;
        switch(key)
        {
        case 'w': direction = direction::up; break;
        case 'a': direction = direction::left; break;
        case 's': direction = direction::down; break;
        case 'd': direction = direction::right; break;
        }
        snake.update(direction);
    }
    std::cout << "you died!, bruh...\n";
}