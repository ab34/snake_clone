# luna
this is a small project to remake snake for my friend, mo.

![gameplay](img/gameplay.png)

## how to use
once you run the application it will print the game-grid to the console and wait for you to add a keyboard-input, followed by pressing `enter`:
```
a -> left
s -> down
d -> right
w -> up
```
after you input your desired direction to move it will print the next game-state and so on.

## how to compile?
this project uses the [meson buildsystem](https://mesonbuild.com/) for compiling
commands for compilation (in root directory of project):
- `meson setup <build_directory_name>`
- `cd <build_directory_name> && meson compile`
- `./snake`

## licence
this project is released under the [gnu affero general public license](https://www.gnu.org/licenses/agpl-3.0.en.html)