#pragma once

struct position
{
    u16 x;
    u16 y;
};

enum direction
{
    up = 0b00,
    left = 0b01,
    down = 0b10,
    right = 0b11
};

class snake_node
{
public:
    snake_node(position pos);
    snake_node(position pos, snake_node* next);
    ~snake_node();
    snake_node* operator=(const snake_node& copy);
    position pos;
    snake_node* next;
};

enum cell
{
    nothing = 0,
    apple = 1,
    snake_body = 3
};


class snake
{
public:
    snake(u16 width, u16 height);
    ~snake();
    void update(direction direction);
    void print();
    cell at(u16 x, u16 y);
    bool alive;
private:
    u16 _round;
    u16 _width;
    u16 _height;
    position _apple;
    snake_node* _snake_head;
};